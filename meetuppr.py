import requests
import time
import json
import datetime
from collections import defaultdict

# API call
# Parameter:
#   t (int) - time in seconds stream is expected to be read for.
# Return:
#   total (int) - total number of rsvp's received
#   efDate (datetime) - date of event furthest into the future
#   efUrl (str) - url of event furthest into the future
#   findCountries() (dict) - the top 3 number of rsvp's received per event host-country
def apiStream(t):
    streamRaw = requests.get("http://stream.meetup.com/2/rsvps", stream = True)
    endTime = time.time() + t
    
    total = 0
    countryList = []
    efDate = 0
    efUrl = None

    # Assumption #1: We will not be saving data, and will prefer to only go through the stream data once.
    for line in streamRaw.iter_lines():
        lineDict = json.loads(line)
        
        # Pulling of host-country data for use in findCountries()
        countryList.append(lineDict['group']['group_country'])

        # Total rsvp counter
        total += 1

        # Furthest future event determination
        if lineDict['event']['time'] > efDate:
            efDate = lineDict['event']['time']
            efUrl = lineDict['event']['event_url']

        # Time management.
        #   NOTE: Time is only checked on each line(rsvp). If there is a gap in time between rsvp's 
        #           pulled from the api, the actual time for the stream read will go on for longer 
        #           than the configured time until another rsvp is read.
        if time.time() >= endTime:
            break
    
    # Unix time conversion to readable datetime
    efDate = datetime.datetime.utcfromtimestamp(efDate / 1000).strftime('%Y-%m-%d %H:%M:%S')
    
    return total, efDate, efUrl, findCountries(countryList)[:3] 

# Host-country frequency sorting
# Paramter:
#   cl (list) - list of each host-country for every read rsvp
# Return:
#   cc (dict) - a sorted dictionary of the county code (key) and the number of occurrences (value). Descending order.
def findCountries(cl):
    cc = defaultdict(int)
    for c in cl:
        cc[c] += 1
    return sorted(cc.items(), key= lambda v: v[1], reverse = True)

# Main. Time configuration and final output
def main():
    results = []
    result = ""
    
    # Assumption #2: We want the time period to be configurable in the executable, not just alterable in the code.
    i = input("Seconds connected to stream: ")
    try:
        i = int(i)
    except:
        print("Defaulting to 60 seconds...")
        i = 60
    try:
        results = apiStream(i)
    except:
        print("Api read error.")
        return None

    # Result output string formatting
    temp = len(results[3])
    if temp == 3:
        result = "{},{},{},{},{},{},{},{},{}".format(
                results[0],results[1],results[2],
                results[3][0][0],results[3][0][1],
                results[3][1][0],results[3][1][1],
                results[3][2][0],results[3][2][1])
    elif temp == 2:
        result = "{},{},{},{},{},{},{}".format(
                results[0],results[1],results[2],
                results[3][0][0],results[3][0][1],
                results[3][1][0],results[3][1][1])
    elif temp == 1:
        result = "{},{},{},{},{}".format(
                results[0],results[1],results[2],
                results[3][0][0],results[3][0][1])
    else:
        print("Result string formatting error.")
        return None
    print(result)
    return None

main()
