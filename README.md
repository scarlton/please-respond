# Please Respond

> Your marketing firm is tracking popularity of events around the world. You have been tasked with tracking down RSVPs for events on a popular online service that helps to organize events, Meetup.com.

## Solution By: Scott Carlton

### Input Format
Your program should connect to the Meetup.com RSVP HTTP data stream at http://stream.meetup.com/2/rsvps

- Meetup RSVP reference: https://www.meetup.com/meetup_api/docs/stream/2/rsvps/

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify the aggregate calculated information in a comma-delimited format.

```
total,future_date,future_url,co_1,co_1_count,co_2,co_2_count,co_3,co_3_count
```

The program will output to screen or console (and not to a file). 

### Project Build Details
This project was run on windows 10 using Anaconda 4.6.8. The Anaconda environment used python 3.7.2 with pip 19.0.3 as the package manager. Libraries used for this project include requests, time, json, datetime, and collection's defaultdict.
